﻿using Plugin.ExternalMaps;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MapMyPad.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public DelegateCommand LaunchMapButtonCommand { get; set; }

        private string _address;
        public string Address
        {
            get { return _address; }
            set { SetProperty(ref _address, value); }
        }

        private string _city;
        public string City
        {
            get { return _city; }
            set { SetProperty(ref _city, value); }
        }

        private string _state;
        public string State
        {
            get { return _state; }
            set { SetProperty(ref _state, value); }
        }

        private string _zipCode;
        public string ZipCode
        {
            get { return _zipCode; }
            set { SetProperty(ref _zipCode, value); }
        }

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Map My Pad";
            LaunchMapButtonCommand = new DelegateCommand(LaunchMapButtonTapped);
        }

        private async void LaunchMapButtonTapped()
        {
            string completeAddress = $"{Address}, {City}, {State}  {ZipCode}";

            var mapLaunchSucceeded = await CrossExternalMaps.Current.NavigateTo(
                "My Pad", Address, City, State, ZipCode, string.Empty, string.Empty);


            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(LaunchMapButtonTapped)}:  {completeAddress}\n\tmapLaunchSucceeded={mapLaunchSucceeded}");
        }
    }
}
