# Map My Pad External Maps Launch Plugin Demo #

This small app is designed as a simple example of using the ['Launch Maps Plugin'](https://github.com/jamesmontemagno/LaunchMapsPlugin) by [James Montemagno](https://montemagno.com/). Simply enter an address, click the 'Go!' button, and the user's default maps app should open to the provided location. Note that there is no validation, so if you enter bogus data, the default map app may get somewhat [confuzzled](https://www.macmillandictionary.com/us/dictionary/american/confuzzled).

## Screenshots of Both Platforms
Perhaps the screenshot that should appear below will appear some day... I've checked and re-checked the syntax, and all looks well. If the image isn't showing, you can look at the MapMyPad.png screenshot in the repo.

![Screenshot of this sample app](MapMyPad.png "Map My Pad")

